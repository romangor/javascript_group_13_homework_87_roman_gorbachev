import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { LoginUserData, RegisterUserData, User } from '../models/users.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UsersService {
  constructor(private http: HttpClient){}

  registerUser(user: RegisterUserData){
    return this.http.post<User>(environment.apiUrl + '/users', user);
  }

  loginUser(userData: LoginUserData) {
    return this.http.post<User>(environment.apiUrl + '/users/sessions', userData);
  }

  logout(token: string) {
    return this.http.delete(
      environment.apiUrl + '/users/sessions',
      {headers: new HttpHeaders({'Authorization': token})}
    );

  }
}
