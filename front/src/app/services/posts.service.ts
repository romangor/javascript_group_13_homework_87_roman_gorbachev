import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Post, PostData } from '../models/post.model';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class PostsService {
  constructor(private http: HttpClient) {
  }

  getPosts(){
    return this.http.get<Post[]>(environment.apiUrl + '/posts').pipe(
      map( response => {
        return response.map(postsData => {
          return new Post(
            postsData._id ,
            postsData.date ,
            postsData.author,
            postsData.title,
            postsData.image,
            postsData.description
          )
        })
      })
    )
  }

  getPost(postId: string) {
    return this.http.get<Post>(`${environment.apiUrl}/posts?post=${postId}`)
      .pipe(map(response => {
          return new Post(response._id, response.date, response.author, response.title, response.image, response.description)
        })
      )
  }

  createPost(post: PostData, token:string){
    const dataPost = new FormData();
    Object.keys(post).forEach(key => {
      if (post[key] !== null) {
        dataPost.append(key, post[key]);
      }
    })
    return this.http.post<Post>(environment.apiUrl + '/posts', dataPost, {headers: new HttpHeaders({'Authorization': token})})}
}
