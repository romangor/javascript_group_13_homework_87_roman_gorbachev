export class Post {
  constructor (
    public _id: string,
    public date: string,
    public author: {
      _id: string,
      userName: string,
    },
    public title: string,
    public image: string,
    public description: string
  ){}
}

export interface PostData {
  [key: string]: any;

  title: string,
  author: string | undefined,
  description: string,
  image: File | null
}



export interface ApiPostsData {
  _id: string,
  title: string,
  author: {
    _id: string,
    userName: string
  },
  description: string,
  image: string
}
