export class User {
  constructor(
    public _id: string,
    public email: string,
    public userName: string,
    public token: string
  ){}

}

export interface RegisterUserData {
  email: string,
  password: string,
  userName: string
}

export interface FieldError {
  message: string
}

export interface RegisterError {
  errors: {
    password: FieldError,
    email: FieldError
  }
}

export interface LoginUserData {
  email: string,
  password: string
}

export interface LoginError {
  error: string;
}
