import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../../models/users.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../store/types';
import { NgForm } from '@angular/forms';
import { createPostRequest } from '../../store/posts.actions';
import { PostData } from '../../models/post.model';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.sass']
})
export class PostCreateComponent implements OnInit {
  @ViewChild('f') form!: NgForm;

  user: Observable<null | User>
  userData!: User

  constructor(private store: Store<AppState>) {
    this.user = store.select(state => state.users.user);
  }

  ngOnInit(): void {
    this.user.subscribe( user => {
      this.userData = user!
    })
  }

  createPost() {
    const post: PostData = {
      title: this.form.value.title,
      author: this.userData?._id,
      description: this.form.value.description,
      image: this.form.value.image
    }
    this.store.dispatch(createPostRequest({createPost: post, token: this.userData?.token}));
  }
}
