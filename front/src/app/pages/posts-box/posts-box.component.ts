import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/models/post.model';
import { Observable } from 'rxjs';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { fetchPostRequest, fetchPostsRequest } from '../../store/posts.actions';

@Component({
  selector: 'app-posts-box',
  templateUrl: './posts-box.component.html',
  styleUrls: ['./posts-box.component.sass']
})
export class PostsBoxComponent implements OnInit {
  posts: Observable<Post[]>
  loading: Observable<boolean>
  error: Observable<null | string>

  constructor(private store: Store<AppState>) {
    this.posts = store.select(state => state.posts.posts);
    this.loading = store.select(state => state.posts.fetchLoading);
    this.error = store.select(state => state.posts.fetchError);
  }

  ngOnInit(): void {
    this.store.dispatch(fetchPostsRequest());
  }

}
