import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { RegisterError } from '../../models/users.model';
import { NgForm } from '@angular/forms';
import { AppState } from '../../store/types';
import { Store } from '@ngrx/store';
import { registerUserRequest } from '../../store/users.actions';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.sass']
})
export class RegistrationComponent implements AfterViewInit, OnDestroy {
  @ViewChild('f') form!: NgForm;

  error: Observable<null | RegisterError>;
  loading: Observable<boolean>
  errorSubscription!: Subscription;

  constructor(private store: Store<AppState>) {
    this.error = store.select( state => state.users.registerError);
    this.loading = store.select( state => state.users.registerLoading);
  }

  ngAfterViewInit(): void{
    this.errorSubscription = this.error.subscribe( error => {
      if(error) {
        const message = error.errors.email.message;
        this.form.form.get('email')?.setErrors({errorServer: message});
      } else {
        this.form.form.get('email')?.setErrors({});
      }
    });
  }
  onSubmit(){
    this.store.dispatch(registerUserRequest({userData: this.form.value}));
  }

  ngOnDestroy(){
    this.errorSubscription.unsubscribe();
  }
}
