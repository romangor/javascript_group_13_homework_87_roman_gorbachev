import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { User } from '../../models/users.model';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store/types';
import { Post } from '../../models/post.model';
import { fetchPostRequest } from '../../store/posts.actions';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.sass']
})
export class PostDetailsComponent implements OnInit, OnDestroy {
  user: Observable<null | User>
  post: Observable<Post>
  loading: Observable<boolean>
  error: Observable<null | string>
  subscriptions!: Subscription;
  constructor(private store: Store<AppState>,  private route: ActivatedRoute) {
    this.user = store.select(state => state.users.user);
    this.post = store.select(state => state.posts.post);
    this.loading = store.select(state => state.posts.fetchPostLoading);
    this.error = store.select(state => state.posts.fetchPostError);
  }

  ngOnInit(): void {
    let postId = '';
    this.subscriptions = this.route.queryParams.subscribe(params => {
      postId = params['post'];
    });
    this.store.dispatch(fetchPostRequest({postId}));
  }

  ngOnDestroy(){
    this.subscriptions.unsubscribe();
  }
}
