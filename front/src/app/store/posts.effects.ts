import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { PostsService } from '../services/posts.service';
import {
  createPostFailure,
  createPostRequest, createPostSuccess,
  fetchPostFailure,
  fetchPostRequest,
  fetchPostsFailure,
  fetchPostsRequest,
  fetchPostsSuccess,
  fetchPostSuccess
} from './posts.actions';
import { catchError, mergeMap, of, tap } from 'rxjs';
import { map } from 'rxjs/operators';
import { HelperService } from '../services/helper.service';
import { Router } from '@angular/router';

@Injectable()
export class PostsEffects {
  constructor(private actions: Actions,
              private postsService: PostsService,
              private helpers: HelperService,
              private router: Router
              ) {}

  fetchPosts = createEffect(() => this.actions.pipe(
    ofType(fetchPostsRequest),
    mergeMap(() => this.postsService.getPosts().pipe(
      map(posts => fetchPostsSuccess({posts})),
      catchError(() => of(fetchPostsFailure({error: "error"}))
    ))
  )))

  fetchPost = createEffect(() => this.actions.pipe(
    ofType(fetchPostRequest),
    mergeMap(({postId}) => this.postsService.getPost(postId).pipe(
      map(post => fetchPostSuccess({post})),
      catchError(() => of(fetchPostFailure({error: "error"}))
      ))
    )))

  createPost = createEffect(() => this.actions.pipe(
    ofType(createPostRequest),
    mergeMap(({createPost, token}) => this.postsService.createPost(createPost, token).pipe(
      map(() => createPostSuccess()),
      tap(() => {
        this.helpers.openSnackBar('Post created')
        void  this.router.navigate(['/']);
      }),
      this.helpers.catchServerError(createPostFailure)
    ))
  ))
}
