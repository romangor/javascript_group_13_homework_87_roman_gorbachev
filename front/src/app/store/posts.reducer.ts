import { PostsState } from './types';
import { createReducer, on } from '@ngrx/store';
import {
  createPostFailure,
  createPostRequest, createPostSuccess,
  fetchPostFailure,
  fetchPostRequest,
  fetchPostsFailure,
  fetchPostsRequest,
  fetchPostsSuccess,
  fetchPostSuccess
} from './posts.actions';


const initialState: PostsState = {
  posts: [],
  post: {_id: '', date: '', title: '', author: {_id: '', userName: ''}, description: '', image: ''},
  createPost: {title: '', author: '', description: '', image: null},
  fetchLoading: false,
  fetchError: null,
  fetchPostLoading: false,
  fetchPostError: null,
  createPostLoading: false,
  createPostError: null
}

export const postsReducer = createReducer(
  initialState,
  on(fetchPostsRequest, state => ({...state, fetchLoading: true})),
  on(fetchPostsSuccess, (state, {posts}) => ({...state, fetchLoading: false, posts})),
  on(fetchPostsFailure, (state, {error}) => ({...state, fetchLoading: true, fetchError: error})),
  on(fetchPostRequest, (state, {postId}) => ({...state, fetchLoading: true, postId})),
  on(fetchPostSuccess, (state, {post}) => ({...state, fetchLoading: false, post})),
  on(fetchPostFailure, (state, {error}) => ({...state, fetchLoading: true, fetchError: error})),
  on(createPostRequest, (state, {createPost, token}) => ({...state, createPostLoading: true, createPost, token})),
  on(createPostSuccess, state => ({...state, createPostLoading: false})),
  on(createPostFailure, (state, {error}) => ({...state, createPostLoading: true, createPostError: error}))
)
