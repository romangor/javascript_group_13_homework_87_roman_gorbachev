import { createAction, props } from '@ngrx/store';
import { Post, PostData } from '../models/post.model';

export const fetchPostsRequest = createAction('[Posts] Fetch Request');
export const fetchPostsSuccess = createAction('[Posts] Success Request', props<{posts: Post[]}>());
export const fetchPostsFailure = createAction('[Posts] Failure Request', props<{error: string}>());
export const fetchPostRequest = createAction('[Post] Fetch Request', props<{postId: string}>());
export const fetchPostSuccess = createAction('[Post] Success Request', props<{post: Post}>());
export const fetchPostFailure = createAction('[Post] Failure Request', props<{error: string}>());
export const createPostRequest = createAction('[Post] createFetch Request', props<{createPost: PostData, token: string}>());
export const createPostSuccess = createAction('[Post] createSuccess Request');
export const createPostFailure = createAction('[Post] createFailure Request', props<{error: string}>());


