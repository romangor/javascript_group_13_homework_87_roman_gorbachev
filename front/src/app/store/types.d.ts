import { Post, PostData } from "../models/post.model";
import { LoginError, RegisterError, User } from '../models/users.model';
export type PostsState = {
  posts: Post[],
  post: Post,
  createPost: PostData,
  fetchLoading: boolean,
  fetchError: string | null,
  fetchPostLoading: boolean,
  fetchPostError: string | null,
  createPostLoading: boolean
  createPostError: string | null
}

export type  UsersState = {
  user: null | User,
  registerLoading: boolean,
  registerError: null | RegisterError,
  loginLoading: boolean,
  loginError: null | LoginError
}

export type AppState = {
  posts: PostsState
  users: UsersState
}
