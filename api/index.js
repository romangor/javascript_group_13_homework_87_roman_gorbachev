const express = require('express');
const cors = require('cors');
const mongoose = require("mongoose");
const config = require('./config');
const app = express();
const posts = require('./app/posts');
const users = require('./app/users');
const comments = require('./app/comments');

const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use(express.static('public'));
app.use('/posts', posts);
app.use('/users', users);
app.use('/comments', comments);

const run = async () => {
    await mongoose.connect(config.mongo.db, config.mongo.options);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    process.on('exit', () => {
        mongoose.disconnect();
    });
};

run().catch(e => console.error(e));