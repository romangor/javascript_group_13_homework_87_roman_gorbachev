const express = require("express");
const auth = require("../middlewere/auth");
const Comment = require("../models/Comment");

const router = express.Router();

router.post('/', auth, async (req, res, next) => {
    try {
        console.log(req.body, req.user);
        const data = {
            author: req.user._id,
            post: req.body.post,
            text: req.body.text
        }
        const comment = new Comment(data);
        await comment.save();
        res.send(comment);
    } catch (e) {
        next(e);
    }
});


module.exports = router;