const express = require('express');
const mongoose = require("mongoose");
const User = require("../models/User");


const router = express.Router();

router.get('/', async (req, res) =>{
    const users = await User.find();
    res.send(users);
})

router.post('/', async (req, res, next) => {
    try{
        const userData = new User(req.body);
        userData.generateToken();
        await userData.save();
        res.send(userData);
    } catch (e) {
        if (e instanceof mongoose.Error.ValidationError) {
            return res.status(400).send(e);
        }

        return next(e);
    }
});

router.post('/sessions',async (req, res) => {
    const user = await User.findOne({email: req.body.email});

    if (!user) {
        return res.status(400).send({error: 'Email not found'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(400).send({error: 'Password is wrong'});
    }

    user.generateToken();
    await user.save();

    return res.send(user);
});

router.delete('/sessions', async (req, res, next) => {
    try {
        const token = req.get('Authorization');
        const success = {message: 'Success'};

        if (!token) return res.send(success);

        const user = await User.findOne({token});

        if (!user) return res.send(success);

        user.generateToken();
        user.save();

        return res.send(success);
    } catch (e) {
        next(e);
    }
});




module.exports = router;