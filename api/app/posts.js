const express = require('express');
const multer = require('multer');
const path = require('path');
const { nanoid } = require('nanoid');
const config = require('../config');
const Post = require("../models/Post");
const Comment = require("../models/Comment");
const auth = require('../middlewere/auth')

const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
    try{
        if(req.query.post){
            const post = await Post.findById(req.query.post).populate('author', '_id userName');
            return res.send(post);
        } else {
            const sort = {}
            sort._id = -1
            const posts = await Post.find().sort(sort).populate('author', '_id userName');
            return res.send(posts);
        }

    } catch (e) {
        next(e);
    }
});

router.post('/', auth, upload.single('image'), async (req, res, next) => {
    try{
        const date = new Date().toISOString();
        console.log(req.user);
        const postData = {
            author: req.user._id,
            title: req.body.title,
            date: date,
            description: req.body.description,
            image: null,
        };

        if (req.file) {
            postData.image = req.file.filename;
        }
        const post = new Post(postData);
        await post.save();
        return res.send(post);
    } catch (e) {
        next(e);
    }
});

module.exports = router;