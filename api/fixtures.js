const mongoose = require('mongoose');
const config = require("./config");
const User = require("./models/User");
const Post = require("./models/Post");

const run = async () => {
    await mongoose.connect(config.mongo.db, config.mongo.options);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    const [roman, ivan] = await User.create({
        email: 'test@example.com',
        userName: 'roman',
        password: '123'
    }, {
        email: 'test1@example.com',
        userName: 'Ivan',
        password: '123'
    }
    );

    await Post.create({
        author: roman,
        title: 'Hello world',
        date: '2022-03-10T06:56:17.943Z',
        description: 'Some description',
        image: 'T2KSfRgzndVWb1m-5cbK4.jpeg'
    }, {
        author: roman,
        title: 'Hello world',
        date: '2021-04-10T06:56:17.943Z',
        description: 'Some description2',
        image: 'T2KSfRgzndVWb1m-5cbK4.jpeg'
    }, {
        author: ivan,
        title: 'Hello world',
        date: '2021-04-10T06:56:17.943Z',
        description: 'Some description2',
        image: 'h5PoifhEQ6LcU7wRm6Yy9.jpeg'
    });

    await mongoose.connection.close();
};

run().catch(e => console.error(e));